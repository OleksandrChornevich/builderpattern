package chornevich.javaweb.task1.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnect {

	public Connection getConnection(String dBUrl, String dBUser, String dBPassword) {

		Connection connection;
		try {
			connection = DriverManager.getConnection(dBUrl, dBUser, dBPassword);
			Class.forName("org.postgresql.Driver");
			return connection;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;

	}

}
