package chornevich.javaweb.task1.model;

public class DirectorExperience {
	private ExperienceBuilder experienceBuilder;
	
	public void setExperienceBuilder(ExperienceBuilder experienceBuilder) {
		this.experienceBuilder = experienceBuilder;
	}
	
	public Experience getExperience() {
		return experienceBuilder.getExperience();
	}
	
	public void constractExperience() {
		experienceBuilder.createNewExperience();
		experienceBuilder.buildIdExperience();
		experienceBuilder.buildStart();
		experienceBuilder.buildEnd();
		experienceBuilder.buildPlace();
		
	}

}
