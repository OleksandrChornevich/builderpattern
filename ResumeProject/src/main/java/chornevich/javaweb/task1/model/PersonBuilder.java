package chornevich.javaweb.task1.model;

public abstract class PersonBuilder {
	
	protected Person person;
	
	public Person getPerson() {
		return person;
	}
	
	public void createNewPerson() {
		person = new Person();
	}
	
	public abstract void buildIdPerson();
	public abstract void buildFirstName();
	public abstract void buildlastName();
	public abstract void buildDateOfBirth();
	public abstract void buildCity();
	public abstract void buildAddress();
	public abstract void buildMobileNumber();
	public abstract void buildeMail();
	public abstract void buildMapOfLanguageSkill();
	public abstract void buildEducations();
	public abstract void buildJobs();

}
