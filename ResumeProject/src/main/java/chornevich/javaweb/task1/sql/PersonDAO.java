package chornevich.javaweb.task1.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import chornevich.javaweb.task1.model.Person;

public class PersonDAO {

	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Person addPerson(Person person) {

		String addPersonSQL = "INSERT INTO public.person"
				+ "(\"firstName\",\"lastName\",\"dateOfBirth\",\"city\",\"address\",\"mobilenumber\",\"email\") VALUES"
				+ "(?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(addPersonSQL, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, person.getFirstName());
			preparedStatement.setString(2, person.getLastName());
			preparedStatement.setDate(3,
					java.sql.Date.valueOf(person.getDateOfBirth().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setString(4, person.getCity());
			preparedStatement.setString(5, person.getAddress());
			preparedStatement.setString(6, person.getMobileNumber());
			preparedStatement.setString(7, person.geteMail());
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				person.setIdPerson(rs.getInt("idperson"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return person;

	}

	public Person getPersonForId(int idPerson) {

		Person person = new Person();

		String selectPersonSQL = "SELECT * FROM public.person WHERE \"idperson\" = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectPersonSQL);
			preparedStatement.setInt(1, idPerson);

			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				person.setFirstName(result.getString("firstName"));
				person.setLastName(result.getString("lastName"));
				person.setDateOfBirth(
						LocalDate.parse((result.getString("dateOfBirth")), DateTimeFormatter.ISO_LOCAL_DATE));
				person.setIdPerson(result.getInt("idperson"));
				person.setCity(result.getString("city"));
				person.setAddress(result.getString("address"));
				person.setMobileNumber(result.getString("mobilenumber"));
				person.seteMail(result.getString("email"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return person;

	}

	public List<Person> getAllPersons() {

		List<Person> persons = new ArrayList<>();

		String selectPersonsSQL = "SELECT * FROM public.person";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectPersonsSQL);
			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				Person person = new Person();
				person.setIdPerson(result.getInt("idperson"));
				person.setFirstName(result.getString("firstName"));
				person.setLastName(result.getString("lastName"));
				person.setCity(result.getString("city"));
				person.setAddress(result.getString("address"));
				person.setDateOfBirth(
						LocalDate.parse((result.getString("dateOfBirth")), DateTimeFormatter.ISO_LOCAL_DATE));
				person.setMobileNumber(result.getString("mobilenumber"));
				person.seteMail(result.getString("email"));
				persons.add(person);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return persons;

	}

	public void deletePersonForId(int idPerson) {

		String deletePersonSQL = "DELETE FROM public.person WHERE \"idperson\" =? ";
		int countOfrow = 0;
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(deletePersonSQL);
			preparedStatement.setInt(1, idPerson);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void deleteAllDataFromDB() {

		String deleteAllSQL = "DELETE FROM public.person ";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(deleteAllSQL);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void updatePerson(Person person) {

		String updatePersonSQL = "UPDATE public.person SET "
				+ "(\"firstName\",\"lastName\",\"dateOfBirth\",\"city\",\"address\",\"mobilenumber\",\"email\")=(?,?,?,?,?,?,?)"
				+ "WHERE \"idperson\" = ?";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(updatePersonSQL);
			preparedStatement.setString(1, person.getFirstName());
			preparedStatement.setString(2, person.getLastName());
			preparedStatement.setDate(3,
					java.sql.Date.valueOf(person.getDateOfBirth().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setString(4, person.getCity());
			preparedStatement.setString(5, person.getAddress());
			preparedStatement.setString(6, person.getMobileNumber());
			preparedStatement.setString(7, person.geteMail());
			preparedStatement.setInt(8, person.getIdPerson());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
