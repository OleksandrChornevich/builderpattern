package chornevich.javaweb.task1.test.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import chornevich.javaweb.task1.model.Person;
import chornevich.javaweb.task1.sql.DBconnect;
import chornevich.javaweb.task1.sql.PersonDAO;

public class TestPersonDAO {

	public Person createTestPerson() {
		Person person = new Person("Dmitro", "Dmitrenko",
				LocalDate.parse("1990-11-16", DateTimeFormatter.ISO_LOCAL_DATE), "Dnipro", "Dniprovska", "0503333333",
				"Dmitro@gmail.com");

		return person;
	}

	public List<Person> createTestListPersons() {
		Person personDmitro = new Person("Dmitro", "Dmitrenko",
				LocalDate.parse("1990-11-16", DateTimeFormatter.ISO_LOCAL_DATE), "Dnipro", "Dniprovska", "0503333333",
				"Dmitro@gmail.com");

		Person personAnton = new Person("Anton", "Andrushenko",
				LocalDate.parse("1980-01-01", DateTimeFormatter.ISO_LOCAL_DATE), "Alchevsk", "Avangardna", "0501111111",
				"Anton.A@gmail.com");

		Person personBoris = new Person("Boris", "Beresov",
				LocalDate.parse("1985-11-16", DateTimeFormatter.ISO_LOCAL_DATE), "Beregove", "Bereganska", "0502222222",
				"Boris.b@gmail.com");
		List<Person> persons = new ArrayList<>();

		persons.add(personDmitro);
		persons.add(personAnton);
		persons.add(personBoris);

		return persons;

	}

	public Connection connectToDB() {

		DBconnect dbConnect = new DBconnect();

		String dBUrl = "jdbc:postgresql://localhost:5433/CurriculumVitae";
		String dBUser = "postgres";
		String dBPassword = "1986";

		Connection connection = dbConnect.getConnection(dBUrl, dBUser, dBPassword);
		return connection;
	}

	@Test
	void testAddGetDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		int idPerson = person.getIdPerson();
		Person getPerson = personDAO.getPersonForId(idPerson);

		Person actual = person;
		Person expected = getPerson;

		Assert.assertEquals(actual, expected);

	}

	@Test
	void testGetAllPersonDAO() {

		List<Person> persons = createTestListPersons();
		List<Person> personsTest = new ArrayList<>();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());

		personDAO.deleteAllDataFromDB();

		for (Person person : persons) {
			personDAO.addPerson(person);
		}

		personsTest = personDAO.getAllPersons();

		List<Person> actual = persons;
		List<Person> expected = personsTest;

		Assert.assertEquals(actual, expected);
	}

	@Test
	void testUpdateDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		int idPerson = person.getIdPerson();
		Person getPerson = personDAO.getPersonForId(idPerson);

		getPerson.setFirstName("Panas");
		getPerson.setLastName("Ostapenko");
		getPerson.setDateOfBirth(LocalDate.parse("2000-01-15", DateTimeFormatter.ISO_LOCAL_DATE));
		getPerson.setCity("Lviv");
		getPerson.setAddress("Shevchenka 88");
		getPerson.setMobileNumber("0503333333");
		getPerson.seteMail("Panas@gmail.com");

		int idGetPerson = person.getIdPerson();
		personDAO.updatePerson(getPerson);

		Person changePerson = personDAO.getPersonForId(idGetPerson);

		Person actual = getPerson;
		Person expected = changePerson;

		Assert.assertEquals(actual, expected);

	}

	@Test
	void testDeleteDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		int idPerson = person.getIdPerson();
		personDAO.deletePersonForId(idPerson);

		Assert.assertEquals(0, personDAO.getPersonForId(idPerson).getIdPerson());

	}

}
