package chornevich.javaweb.task1.model;

public enum Language {
	ENGLISH, FRENCH, GERMAN
}
