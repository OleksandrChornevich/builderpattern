package chornevich.javaweb.task1.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import chornevich.javaweb.task1.model.Experience;
import chornevich.javaweb.task1.model.Person;

public class PersonJobDAO implements ExperienceDAO {

	private Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public Experience add(Experience job, int idPerson) {
		String insertJobSQL = "INSERT INTO public.job" + "(\"start\",\"end\",\"place\",\"idjob\") VALUES" + "(?,?,?,?)";

		// Connection connection = DAOabstr.
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(insertJobSQL,
					PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setDate(1,
					java.sql.Date.valueOf(job.getStart().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setDate(2, java.sql.Date.valueOf(job.getEnd().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setString(3, job.getPlace());
			preparedStatement.setInt(4, idPerson);
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				job.setIdExperience(rs.getInt("pkjob"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return job;

	}

	@Override
	public Experience get(int pkjob) {
		Experience job = new Experience();
		String selectjobSQL = "SELECT * FROM public.job where \"pkjob\" = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectjobSQL);
			preparedStatement.setInt(1, pkjob);

			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				job.setIdExperience(result.getInt("pkjob"));
				job.setEnd(LocalDate.parse(result.getString("end"), DateTimeFormatter.ISO_LOCAL_DATE));
				job.setStart(LocalDate.parse(result.getString("start"), DateTimeFormatter.ISO_LOCAL_DATE));
				job.setPlace(result.getString("place"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return job;
	}

	public List<Experience> getAll(int idPerson) {

		List<Experience> exp = new ArrayList<>();
		String selectContactSQL = "SELECT * FROM public.job where \"idjob\" = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectContactSQL);
			preparedStatement.setInt(1, idPerson);

			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				Experience job = new Experience();
				job.setIdExperience(result.getInt("pkjob"));
				job.setEnd(LocalDate.parse(result.getString("end"), DateTimeFormatter.ISO_LOCAL_DATE));
				job.setStart(LocalDate.parse(result.getString("start"), DateTimeFormatter.ISO_LOCAL_DATE));
				job.setPlace(result.getString("place"));
				exp.add(job);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return exp;

	}

	@Override
	public void delete(int pkjob) {
		try {
			String deleteJobSQL = "DELETE FROM public.job WHERE \"pkjob\" =? ";

			PreparedStatement preparedStatement = connection.prepareStatement(deleteJobSQL);
			preparedStatement.setInt(1, pkjob);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void update(int pkjob, Experience job) {

		String updateJobSQL = "UPDATE public.job SET " + "(\"start\",\"end\",\"place\")=(?,?,?)"
				+ "WHERE \"pkjob\" = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(updateJobSQL);

			preparedStatement.setDate(1,
					java.sql.Date.valueOf(job.getStart().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setDate(2, java.sql.Date.valueOf(job.getEnd().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setString(3, job.getPlace());
			preparedStatement.setInt(4, pkjob);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
}
