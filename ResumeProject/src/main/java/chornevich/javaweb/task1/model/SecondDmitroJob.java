package chornevich.javaweb.task1.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SecondDmitroJob extends ExperienceBuilder {

	@Override
	public void buildIdExperience() {
		experience.setIdExperience(2);
		
	}

	@Override
	public void buildStart() {
		experience.setStart(LocalDate.parse("2019-06-01", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildEnd() {
		experience.setEnd(LocalDate.parse("2025-07-01", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildPlace() {
		experience.setPlace("BestPlace");
		
	}
	

}
