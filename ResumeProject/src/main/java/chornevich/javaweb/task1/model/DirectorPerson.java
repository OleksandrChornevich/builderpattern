package chornevich.javaweb.task1.model;

public class DirectorPerson {
	private PersonBuilder personBuilder;
	
	public void setPersonBuilder(PersonBuilder personBuilder) {
		this.personBuilder = personBuilder;
	}
	
	public Person getPerson() {
		return personBuilder.getPerson();
	}
	
	public void constractPerson() {
		personBuilder.createNewPerson();
		personBuilder.buildIdPerson();
		personBuilder.buildFirstName();
		personBuilder.buildlastName();
		personBuilder.buildDateOfBirth();
		personBuilder.buildCity();
		personBuilder.buildAddress();
		personBuilder.buildMobileNumber();
		personBuilder.buildeMail();
		personBuilder.buildMapOfLanguageSkill();
		personBuilder.buildEducations();
		personBuilder.buildJobs();
	}

}
