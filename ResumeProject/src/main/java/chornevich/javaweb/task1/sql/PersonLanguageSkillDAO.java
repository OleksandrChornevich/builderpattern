package chornevich.javaweb.task1.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import chornevich.javaweb.task1.model.Cefractfl;
import chornevich.javaweb.task1.model.Language;
import chornevich.javaweb.task1.model.Person;

public class PersonLanguageSkillDAO {

	private Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void add(Language key, Cefractfl value, int idPerson) {

		String selectLangSkillSQL = "INSERT INTO public.languageskill (\"language\",\"cefractfl\",\"idlanguageskill\") VALUES(?::languageenum, ?::cefractflenum, ?)";
		try {

			PreparedStatement preparedStatement = connection.prepareStatement(selectLangSkillSQL);

			preparedStatement.setString(1, key.toString());
			preparedStatement.setString(2, value.toString());
			preparedStatement.setInt(3, idPerson);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Cefractfl get(int idPerson, Language key) {

		try {
			String selectLangSkillSQL = "SELECT * FROM public.languageskill where \"idlanguageskill\" = ? and \"language\"=?::languageenum";
			PreparedStatement preparedStatement = connection.prepareStatement(selectLangSkillSQL);
			preparedStatement.setInt(1, idPerson);
			preparedStatement.setString(2, key.toString());
			Cefractfl value = null;
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				value = Cefractfl.valueOf(result.getString("cefractfl"));
			}

			return value;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Map<Language, Cefractfl> getAllLanguageSkills(int idPerson) {

		Map<Language, Cefractfl> langSkill = new TreeMap<>();
		try {

			String selectContactSQL = "SELECT * FROM public.languageskill where \"idlanguageskill\" = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(selectContactSQL);
			preparedStatement.setInt(1, idPerson);

			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				langSkill.put(Language.valueOf(result.getString("language")),
						Cefractfl.valueOf(result.getString("cefractfl")));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return langSkill;

	}

	public void delete(int idPerson, Language key) {

		String deleteLangSkillSQL = "DELETE FROM public.languageskill WHERE \"idlanguageskill\" =? and \"language\"=?::languageenum";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(deleteLangSkillSQL);
			preparedStatement.setInt(1, idPerson);
			preparedStatement.setString(2, key.toString());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(int idPerson, Language key, Cefractfl value) {

		try {
			String updateLangSkillSQL = "UPDATE public.languageskill SET "
					+ "(\"cefractfl\")=ROW(?::cefractflenum) WHERE \"idlanguageskill\" = ? and \"language\" = ?::languageenum";

			PreparedStatement preparedStatement = connection.prepareStatement(updateLangSkillSQL);

			preparedStatement.setString(1, value.toString());
			preparedStatement.setInt(2, idPerson);
			preparedStatement.setString(3, key.toString());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
