package chornevich.javaweb.task1.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import chornevich.javaweb.task1.model.Experience;
import chornevich.javaweb.task1.model.Person;

public class PersonEducationDAO implements ExperienceDAO {

	private Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public Experience add(Experience education, int idPerson) {
		String insertEducationSQL = "INSERT INTO public.education"
				+ "(\"start\",\"end\",\"place\",\"ideducation\") VALUES" + "(?,?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(insertEducationSQL,
					PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setDate(1,
					java.sql.Date.valueOf(education.getStart().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setDate(2,
					java.sql.Date.valueOf(education.getEnd().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setString(3, education.getPlace());
			preparedStatement.setInt(4, idPerson);
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				education.setIdExperience(rs.getInt("pkeducation"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return education;

	}

	@Override
	public Experience get(int pkeducation) {
		Experience education = new Experience();
		String selectEducationSQL = "SELECT * FROM public.education where \"pkeducation\" = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectEducationSQL);
			preparedStatement.setInt(1, pkeducation);

			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {

				education.setIdExperience(result.getInt("pkeducation"));
				education.setEnd(LocalDate.parse(result.getString("end"), DateTimeFormatter.ISO_LOCAL_DATE));
				education.setStart(LocalDate.parse(result.getString("start"), DateTimeFormatter.ISO_LOCAL_DATE));
				education.setPlace(result.getString("place"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return education;
	}

	public List<Experience> getAll(int idPerson) {

		List<Experience> exp = new ArrayList<>();
		String selectContactSQL = "SELECT * FROM public.education where \"ideducation\" = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectContactSQL);
			preparedStatement.setInt(1, idPerson);

			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				Experience education = new Experience();
				education.setIdExperience(result.getInt("pkeducation"));
				education.setEnd(LocalDate.parse(result.getString("end"), DateTimeFormatter.ISO_LOCAL_DATE));
				education.setStart(LocalDate.parse(result.getString("start"), DateTimeFormatter.ISO_LOCAL_DATE));
				education.setPlace(result.getString("place"));
				exp.add(education);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return exp;

	}

	@Override
	public void delete(int pkeducation) {
		String deleteEducationSQL = "DELETE FROM public.education WHERE \"pkeducation\" =? ";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(deleteEducationSQL);
			preparedStatement.setInt(1, pkeducation);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void update(int pkeducation, Experience education) {
		String updateEducationSQL = "UPDATE public.education SET " + "(\"start\",\"end\",\"place\")=(?,?,?)"
				+ "WHERE \"pkeducation\" = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(updateEducationSQL);

			preparedStatement.setDate(1,
					java.sql.Date.valueOf(education.getStart().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setDate(2,
					java.sql.Date.valueOf(education.getEnd().format(DateTimeFormatter.ISO_LOCAL_DATE)));
			preparedStatement.setString(3, education.getPlace());
			preparedStatement.setInt(4, education.getIdExperience());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
}
