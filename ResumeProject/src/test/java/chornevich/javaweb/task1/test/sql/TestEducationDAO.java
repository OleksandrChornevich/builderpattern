package chornevich.javaweb.task1.test.sql;

import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import chornevich.javaweb.task1.model.Experience;
import chornevich.javaweb.task1.model.Person;
import chornevich.javaweb.task1.sql.DBconnect;
import chornevich.javaweb.task1.sql.PersonDAO;
import chornevich.javaweb.task1.sql.PersonEducationDAO;
import chornevich.javaweb.task1.sql.PersonJobDAO;

public class TestEducationDAO {

	public Person createTestPerson() {

		Person person = new Person("Dmitro", "Dmitrenko",
				LocalDate.parse("1990-11-16", DateTimeFormatter.ISO_LOCAL_DATE), "Dnipro", "Dniprovska", "0503333333",
				"Dmitro@gmail.com");

		List<Experience> listOfExp = new ArrayList<>();
		Experience job1 = new Experience(LocalDate.parse("2013-07-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2019-05-31", DateTimeFormatter.ISO_LOCAL_DATE), "School 1");
		Experience job2 = new Experience(LocalDate.parse("2019-06-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2025-07-01", DateTimeFormatter.ISO_LOCAL_DATE), "DhNU");

		listOfExp.add(job1);
		listOfExp.add(job2);
		person.setJobs(listOfExp);

		return person;
	}

	public Connection connectToDB() {

		DBconnect dbConnect = new DBconnect();

		String dBUrl = "jdbc:postgresql://localhost:5433/CurriculumVitae";
		String dBUser = "postgres";
		String dBPassword = "1986";

		Connection connection = dbConnect.getConnection(dBUrl, dBUser, dBPassword);
		return connection;
	}

	@Test
	public void testAddGetDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		PersonEducationDAO educationDAO = new PersonEducationDAO();
		educationDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		List<Experience> actual = new ArrayList<>();

		actual.add(educationDAO.add(person.getJobs().get(0), idPerson));
		actual.add(educationDAO.add(person.getJobs().get(1), idPerson));

		List<Experience> testList = new ArrayList<>();
		testList.add(educationDAO.get(person.getJobs().get(0).getIdExperience()));
		testList.add(educationDAO.get(person.getJobs().get(1).getIdExperience()));

		actual = person.getJobs();
		List<Experience> expected = testList;

		Assert.assertEquals(actual, expected);

	}

	@Test
	public void testGetAllDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		PersonEducationDAO educationDAO = new PersonEducationDAO();
		educationDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		List<Experience> actual = new ArrayList<>();

		actual.add(educationDAO.add(person.getJobs().get(0), idPerson));
		actual.add(educationDAO.add(person.getJobs().get(1), idPerson));

		List<Experience> testList = new ArrayList<>();
		testList = educationDAO.getAll(idPerson);

		actual = person.getJobs();

		List<Experience> expected = testList;

		Assert.assertEquals(actual, expected);

	}

	@Test
	public void testUpdateDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		PersonEducationDAO educationDAO = new PersonEducationDAO();
		educationDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		List<Experience> actual = new ArrayList<>();

		actual.add(educationDAO.add(person.getJobs().get(0), idPerson));
		actual.add(educationDAO.add(person.getJobs().get(1), idPerson));

		actual = educationDAO.getAll(idPerson);

		actual.get(0).setStart(LocalDate.parse("1999-03-01", DateTimeFormatter.ISO_LOCAL_DATE));
		actual.get(0).setEnd(LocalDate.parse("2019-05-31", DateTimeFormatter.ISO_LOCAL_DATE));
		actual.get(0).setPlace("School 7");
		actual.get(1).setStart(LocalDate.parse("2019-06-01", DateTimeFormatter.ISO_LOCAL_DATE));
		actual.get(1).setEnd(LocalDate.parse("2030-06-01", DateTimeFormatter.ISO_LOCAL_DATE));
		actual.get(1).setPlace("Academia");

		educationDAO.update(actual.get(0).getIdExperience(), actual.get(0));
		educationDAO.update(actual.get(1).getIdExperience(), actual.get(1));

		List<Experience> testList = new ArrayList<>();

		testList = educationDAO.getAll(idPerson);

		List<Experience> expected = testList;

		Assert.assertEquals(actual, expected);

	}

	@Test
	public void testDeleteDAO() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		PersonEducationDAO educationDAO = new PersonEducationDAO();
		educationDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		List<Experience> actual = new ArrayList<>();

		actual.add(educationDAO.add(person.getJobs().get(0), idPerson));
		actual.add(educationDAO.add(person.getJobs().get(1), idPerson));

		actual = educationDAO.getAll(idPerson);

		educationDAO.delete(actual.get(0).getIdExperience());

		Experience expected = educationDAO.get(actual.get(0).getIdExperience());

		Assert.assertEquals(0, expected.getIdExperience());

	}

}
