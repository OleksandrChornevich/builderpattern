package chornevich.javaweb.task1.model;

public abstract class ExperienceBuilder {
	protected Experience experience;
	
	public Experience getExperience() { 
		return experience;
	}
	
	public void createNewExperience() {
		experience = new Experience();
	}
	
	public abstract void buildIdExperience();
	public abstract void buildStart();
	public abstract void buildEnd();
	public abstract void buildPlace();
}
