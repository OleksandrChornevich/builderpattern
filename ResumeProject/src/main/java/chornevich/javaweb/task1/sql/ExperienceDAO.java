package chornevich.javaweb.task1.sql;

import java.sql.Connection;
import java.sql.SQLException;

import chornevich.javaweb.task1.model.Experience;

public interface ExperienceDAO {

	public Experience add(Experience exp, int id);

	public Experience get(int id);

	public void delete(int id);

	public void update(int pkgob, Experience exp);

}
