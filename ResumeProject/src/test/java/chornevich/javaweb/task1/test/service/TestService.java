package chornevich.javaweb.task1.test.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import chornevich.javaweb.task1.model.Cefractfl;
import chornevich.javaweb.task1.model.Experience;
import chornevich.javaweb.task1.model.Language;
import chornevich.javaweb.task1.model.Person;
import chornevich.javaweb.task1.service.PersonService;

import org.testng.Assert;
import org.testng.annotations.Test;

class TestImportTxtCV {

	public Person createTestPerson() throws IOException {

		Person person = new Person("Dmitro", "Dmitrenko",
				LocalDate.parse("1990-11-16", DateTimeFormatter.ISO_LOCAL_DATE), "Dnipro", "Dniprovska", "0503333333",
				"Dmitro@gmail.com");

		Map<Language, Cefractfl> langSkill = new TreeMap<>();
		langSkill.put(Language.valueOf("ENGLISH"), Cefractfl.valueOf("C1"));
		langSkill.put(Language.valueOf("FRENCH"), Cefractfl.valueOf("B1"));
		langSkill.put(Language.valueOf("GERMAN"), Cefractfl.valueOf("A1"));
		person.setMapOfLanguageSkill(langSkill);

		List<Experience> listOfEdu = new ArrayList<>();
		Experience persEdu1 = new Experience(LocalDate.parse("1996-09-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2006-06-01", DateTimeFormatter.ISO_LOCAL_DATE), "School 3");
		Experience persEdu2 = new Experience(LocalDate.parse("2007-09-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2012-07-01", DateTimeFormatter.ISO_LOCAL_DATE), "Dhnu");

		listOfEdu.add(persEdu1);
		listOfEdu.add(persEdu2);

		person.setEducations(listOfEdu);

		List<Experience> listOfExp = new ArrayList<>();
		Experience persJob1 = new Experience(LocalDate.parse("2013-07-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2019-05-31", DateTimeFormatter.ISO_LOCAL_DATE), "Firma");
		Experience persJob2 = new Experience(LocalDate.parse("2019-06-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2025-07-01", DateTimeFormatter.ISO_LOCAL_DATE), "BestPlace");

		listOfExp.add(persJob1);
		listOfExp.add(persJob2);

		person.setJobs(listOfExp);

		return person;

	}

	@Test
	void testTXT() throws IOException {

		Person person = createTestPerson();

		PersonService persServTxt = new PersonService();

		Person actual = person;
		Person expected = persServTxt.importTxtFromFile("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.txt");
		Assert.assertEquals(actual, expected);

	}

	@Test
	void testJSON() throws IOException {

		Person person = createTestPerson();

		PersonService persServ = new PersonService();
		persServ.exportJson("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.json", person);

		Person actual = person;
		Person expected = persServ.importJsonFromFile("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.json");
		Assert.assertEquals(actual, expected);

	}

	@Test
	void testXML() throws IOException {

		Person person = createTestPerson();

		PersonService persServ = new PersonService();
		persServ.exportXML("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.xml", person);

		Person actual = person;
		Person expected = persServ.importXMLfromFile("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.xml");
		Assert.assertEquals(actual, expected);

	}

	@Test
	void testYAML() throws IOException {

		Person person = createTestPerson();

		PersonService persServ = new PersonService();
		persServ.exportYaml("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.yml", person);

		Person actual = person;
		Person expected = persServ.importYamlFromFile("/home/oleksandr/Ch-082/Ch82ImportCV/src/test/source/test.yml");
		Assert.assertEquals(actual, expected);

	}

}
