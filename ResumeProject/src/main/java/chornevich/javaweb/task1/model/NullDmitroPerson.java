package chornevich.javaweb.task1.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NullDmitroPerson extends PersonBuilder{

	@Override
	public void buildIdPerson() {
		person.setIdPerson(0);
		
	}

	@Override
	public void buildFirstName() {
		person.setFirstName(null);
		
	}

	@Override
	public void buildlastName() {
		person.setLastName(null);
		
	}

	@Override
	public void buildDateOfBirth() {
		person.setDateOfBirth(null);
		
	}

	@Override
	public void buildCity() {
		person.setCity(null);
		
	}

	@Override
	public void buildAddress() {
		person.setAddress(null);
		
	}

	@Override
	public void buildMobileNumber() {
		person.setMobileNumber(null);
		
	}

	@Override
	public void buildeMail() {
		person.seteMail(null);
		
	}

	@Override
	public void buildMapOfLanguageSkill() {
		Map<Language, Cefractfl> langSkill = new TreeMap<>();
		langSkill.put(Language.valueOf("ENGLISH"), Cefractfl.valueOf("C1"));
		langSkill.put(Language.valueOf("FRENCH"), Cefractfl.valueOf("B1"));
		langSkill.put(Language.valueOf("GERMAN"), Cefractfl.valueOf("A1"));
		
		person.setMapOfLanguageSkill(langSkill);
		
	}

	@Override
	public void buildEducations() {
		List<Experience> listOfEdu = new ArrayList<>();
		Experience persEdu1 = new Experience(LocalDate.parse("1996-09-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2006-06-01", DateTimeFormatter.ISO_LOCAL_DATE), "School 3");
		Experience persEdu2 = new Experience(LocalDate.parse("2007-09-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2012-07-01", DateTimeFormatter.ISO_LOCAL_DATE), "Dhnu");

		listOfEdu.add(persEdu1);
		listOfEdu.add(persEdu2);

		person.setEducations(listOfEdu);
		
	}

	@Override
	public void buildJobs() {
		List<Experience> listOfExp = new ArrayList<>();
		Experience persJob1 = new Experience(LocalDate.parse("2013-07-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2019-05-31", DateTimeFormatter.ISO_LOCAL_DATE), "Firma");
		Experience persJob2 = new Experience(LocalDate.parse("2019-06-01", DateTimeFormatter.ISO_LOCAL_DATE),
				LocalDate.parse("2025-07-01", DateTimeFormatter.ISO_LOCAL_DATE), "BestPlace");

		listOfExp.add(persJob1);
		listOfExp.add(persJob2);

		person.setJobs(listOfExp);		
	}

}
