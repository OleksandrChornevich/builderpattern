package chornevich.javaweb.task1.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DmitroPerson extends PersonBuilder{

	@Override
	public void buildIdPerson() {
		person.setIdPerson(0);
		
	}

	@Override
	public void buildFirstName() {
		person.setFirstName("Dmitro");
		
	}

	@Override
	public void buildlastName() {
		person.setLastName("Ostapenko");
		
	}

	@Override
	public void buildDateOfBirth() {
		person.setDateOfBirth(LocalDate.parse("2000-01-15", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildCity() {
		person.setCity("Lviv");
		
	}

	@Override
	public void buildAddress() {
		person.setAddress("Shevchenka 88");
		
	}

	@Override
	public void buildMobileNumber() {
		person.setMobileNumber("0503333333");
		
	}

	@Override
	public void buildeMail() {
		person.seteMail("Dmitro@gmail.com");
		
	}

	@Override
	public void buildMapOfLanguageSkill() {
		Map<Language, Cefractfl> langSkill = new TreeMap<>();
		langSkill.put(Language.valueOf("ENGLISH"), Cefractfl.valueOf("C1"));
		langSkill.put(Language.valueOf("FRENCH"), Cefractfl.valueOf("B1"));
		langSkill.put(Language.valueOf("GERMAN"), Cefractfl.valueOf("A1"));
		
		person.setMapOfLanguageSkill(langSkill);
		
	}

	@Override
	public void buildEducations() {
		List<Experience> listOfEdu = new ArrayList<>();
		
		DirectorExperience directorExp = new DirectorExperience();
		
		ExperienceBuilder firstDmitroEducation = new FirstDmitroEducation();
		
		directorExp.setExperienceBuilder(firstDmitroEducation);
		directorExp.constractExperience();
		
		Experience edu1 = directorExp.getExperience();
		
		ExperienceBuilder secondDmitroEducation = new SecondDmitroEducation();
		
		directorExp.setExperienceBuilder(secondDmitroEducation);
		directorExp.constractExperience();
		
		Experience edu2 = directorExp.getExperience();
		
		listOfEdu.add(edu1);
		listOfEdu.add(edu2);

		person.setEducations(listOfEdu);
		
	}

	@Override
	public void buildJobs() {
	
		List<Experience> listOfJob = new ArrayList<>();
		
		DirectorExperience directorExp = new DirectorExperience();
		
		ExperienceBuilder firstDmitroJob = new FirstDmitroJob();
		
		directorExp.setExperienceBuilder(firstDmitroJob);
		directorExp.constractExperience();
		
		Experience job1 = directorExp.getExperience();
		
		ExperienceBuilder secondDmitroJob = new SecondDmitroJob();
		
		directorExp.setExperienceBuilder(secondDmitroJob);
		directorExp.constractExperience();
		
		Experience job2 = directorExp.getExperience();
		
		listOfJob.add(job1);
		listOfJob.add(job2);

		person.setJobs(listOfJob);		
	}

}
