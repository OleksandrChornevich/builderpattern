package chornevich.javaweb.task1.test.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import chornevich.javaweb.task1.model.Cefractfl;
import chornevich.javaweb.task1.model.DirectorPerson;
import chornevich.javaweb.task1.model.DmitroPerson;
import chornevich.javaweb.task1.model.Experience;
import chornevich.javaweb.task1.model.ExperienceBuilder;
import chornevich.javaweb.task1.model.PersonBuilder;
import chornevich.javaweb.task1.model.FirstDmitroEducation;
import chornevich.javaweb.task1.model.Language;
import chornevich.javaweb.task1.model.Person;
import chornevich.javaweb.task1.service.PersonService;

import org.testng.Assert;
import org.testng.annotations.Test;

class TestImportTxtCVnew {

	public Person createTestPerson() throws IOException {

		DirectorPerson directorPerson = new DirectorPerson();
		
		PersonBuilder dmitroPerson = new DmitroPerson();
		
		directorPerson.setPersonBuilder(dmitroPerson);	
		directorPerson.constractPerson();
		
		Person person = directorPerson.getPerson();
		
		return person; 

	}

	@Test
	void testTXT() throws IOException {

		Person person = createTestPerson();

		PersonService persServTxt = new PersonService();

		Person actual = person;
		Person expected = persServTxt.importTxtFromFile("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/testNew.txt");
		Assert.assertEquals(actual, expected);

	}

	@Test
	void testJSON() throws IOException {

		Person person = createTestPerson();

		PersonService persServ = new PersonService();
		persServ.exportJson("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/test.json", person);

		Person actual = person;
		Person expected = persServ.importJsonFromFile("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/test.json");
		Assert.assertEquals(actual, expected);

	}

	@Test
	void testXML() throws IOException {

		Person person = createTestPerson();

		PersonService persServ = new PersonService();
		persServ.exportXML("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/test.xml", person);

		Person actual = person;
		Person expected = persServ.importXMLfromFile("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/test.xml");
		Assert.assertEquals(actual, expected);

	}

	@Test
	void testYAML() throws IOException {

		Person person = createTestPerson();

		PersonService persServ = new PersonService();
		persServ.exportYaml("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/test.yml", person);

		Person actual = person;
		Person expected = persServ.importYamlFromFile("/home/oleksandr/JavaProjectsCh082/ch-082/ResumeProject/src/test/source/test.yml");
		Assert.assertEquals(actual, expected);

	}

}
