package chornevich.javaweb.task1.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NullDmitroExperience extends ExperienceBuilder {

	@Override
	public void buildIdExperience() {
		experience.setIdExperience(0);
		
	}

	@Override
	public void buildStart() {
		experience.setStart(null);
		
	}

	@Override
	public void buildEnd() {
		experience.setStart(null);
		
	}

	@Override
	public void buildPlace() {
		experience.setPlace(null);
		
	}
	

}
