package chornevich.javaweb.task1.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FirstDmitroEducation extends ExperienceBuilder {

	@Override
	public void buildIdExperience() {
		experience.setIdExperience(1);
		
	}

	@Override
	public void buildStart() {
		experience.setStart(LocalDate.parse("1996-09-01", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildEnd() {
		experience.setEnd(LocalDate.parse("2006-06-01", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildPlace() {
		experience.setPlace("School 3");
		
	}
	

}
