package chornevich.javaweb.task1.test.sql;

import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import chornevich.javaweb.task1.model.Cefractfl;
import chornevich.javaweb.task1.model.Language;
import chornevich.javaweb.task1.model.Person;
import chornevich.javaweb.task1.sql.DBconnect;
import chornevich.javaweb.task1.sql.PersonDAO;
import chornevich.javaweb.task1.sql.PersonLanguageSkillDAO;

public class TestLanguageSkillDAO {

	public Person createTestPerson() {

		Person person = new Person("Dmitro", "Dmitrenko",
				LocalDate.parse("1990-11-16", DateTimeFormatter.ISO_LOCAL_DATE), "Dnipro", "Dniprovska", "0503333333",
				"Dmitro@gmail.com");

		Map<Language, Cefractfl> langSkill = new TreeMap<>();
		langSkill.put(Language.valueOf("ENGLISH"), Cefractfl.valueOf("C1"));
		langSkill.put(Language.valueOf("FRENCH"), Cefractfl.valueOf("B1"));
		langSkill.put(Language.valueOf("GERMAN"), Cefractfl.valueOf("A1"));
		person.setMapOfLanguageSkill(langSkill);

		return person;
	}

	public Connection connectToDB() {

		DBconnect dbConnect = new DBconnect();

		String dBUrl = "jdbc:postgresql://localhost:5433/CurriculumVitae";
		String dBUser = "postgres";
		String dBPassword = "1986";

		Connection connection = dbConnect.getConnection(dBUrl, dBUser, dBPassword);
		return connection;
	}

	@Test
	public void testAddGetLangSkill() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		PersonLanguageSkillDAO langSkillDAO = new PersonLanguageSkillDAO();
		langSkillDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		Set<Map.Entry<Language, Cefractfl>> langSkillEntry = person.getMapOfLanguageSkill().entrySet();
		Iterator<Map.Entry<Language, Cefractfl>> langSkillIterator = langSkillEntry.iterator();
		while (langSkillIterator.hasNext()) {
			Map.Entry<Language, Cefractfl> entry = langSkillIterator.next();
			langSkillDAO.add(entry.getKey(), entry.getValue(), idPerson);
		}

		Map<Language, Cefractfl> testMap = new TreeMap<>();

		testMap.put(Language.valueOf("ENGLISH"), langSkillDAO.get(idPerson, Language.valueOf("ENGLISH")));
		testMap.put(Language.valueOf("FRENCH"), langSkillDAO.get(idPerson, Language.valueOf("FRENCH")));
		testMap.put(Language.valueOf("GERMAN"), langSkillDAO.get(idPerson, Language.valueOf("GERMAN")));

		Map<Language, Cefractfl> actual = person.getMapOfLanguageSkill();
		Map<Language, Cefractfl> expected = testMap;

		Assert.assertEquals(actual, expected);
	}

	@Test
	public void testGetAllLangSkill() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());

		personDAO.addPerson(person);

		PersonLanguageSkillDAO langSkillDAO = new PersonLanguageSkillDAO();
		langSkillDAO.setConnection(connectToDB());

		Set<Map.Entry<Language, Cefractfl>> langSkillEntry = person.getMapOfLanguageSkill().entrySet();
		Iterator<Map.Entry<Language, Cefractfl>> langSkillIterator = langSkillEntry.iterator();
		while (langSkillIterator.hasNext()) {
			Map.Entry<Language, Cefractfl> entryDmitro = langSkillIterator.next();
			langSkillDAO.add(entryDmitro.getKey(), entryDmitro.getValue(), person.getIdPerson());
		}

		Map<Language, Cefractfl> testMap = new TreeMap<>();

		testMap = langSkillDAO.getAllLanguageSkills(person.getIdPerson());

		Map<Language, Cefractfl> actual = person.getMapOfLanguageSkill();
		Map<Language, Cefractfl> expected = testMap;

		Assert.assertEquals(actual, expected);
	}

	@Test
	public void testUpdateLangSkill() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());

		personDAO.deleteAllDataFromDB();
		personDAO.addPerson(person);

		PersonLanguageSkillDAO langSkillDAO = new PersonLanguageSkillDAO();
		langSkillDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		Set<Map.Entry<Language, Cefractfl>> langSkillEntry = person.getMapOfLanguageSkill().entrySet();
		Iterator<Map.Entry<Language, Cefractfl>> langSkillIterator = langSkillEntry.iterator();
		while (langSkillIterator.hasNext()) {
			Map.Entry<Language, Cefractfl> entry = langSkillIterator.next();
			langSkillDAO.add(entry.getKey(), entry.getValue(), idPerson);
		}

		Map<Language, Cefractfl> updateLocal = new TreeMap<>();

		updateLocal = langSkillDAO.getAllLanguageSkills(person.getIdPerson());

		updateLocal.put(Language.valueOf("ENGLISH"), Cefractfl.valueOf("A1"));
		updateLocal.put(Language.valueOf("FRENCH"), Cefractfl.valueOf("C1"));
		updateLocal.put(Language.valueOf("GERMAN"), Cefractfl.valueOf("B1"));

		langSkillDAO.update(idPerson, Language.valueOf("ENGLISH"), Cefractfl.valueOf("A1"));
		langSkillDAO.update(idPerson, Language.valueOf("FRENCH"), Cefractfl.valueOf("C1"));
		langSkillDAO.update(idPerson, Language.valueOf("GERMAN"), Cefractfl.valueOf("B1"));

		Map<Language, Cefractfl> testMap = langSkillDAO.getAllLanguageSkills(person.getIdPerson());

		Map<Language, Cefractfl> actual = updateLocal;
		Map<Language, Cefractfl> expected = testMap;

		Assert.assertEquals(actual, expected);
	}

	@Test
	public void testDeleteLangSkill() {

		Person person = createTestPerson();

		PersonDAO personDAO = new PersonDAO();
		personDAO.setConnection(connectToDB());
		personDAO.addPerson(person);

		PersonLanguageSkillDAO langSkillDAO = new PersonLanguageSkillDAO();
		langSkillDAO.setConnection(connectToDB());

		int idPerson = person.getIdPerson();

		Set<Map.Entry<Language, Cefractfl>> langSkillEntry = person.getMapOfLanguageSkill().entrySet();
		Iterator<Map.Entry<Language, Cefractfl>> langSkillIterator = langSkillEntry.iterator();
		while (langSkillIterator.hasNext()) {
			Map.Entry<Language, Cefractfl> entry = langSkillIterator.next();
			langSkillDAO.add(entry.getKey(), entry.getValue(), idPerson);
		}

		langSkillDAO.delete(idPerson, Language.valueOf("ENGLISH"));

		Cefractfl expected = langSkillDAO.get(idPerson, Language.valueOf("ENGLISH"));

		Assert.assertNull(expected);
	}

}
