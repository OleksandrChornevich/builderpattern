package chornevich.javaweb.task1.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SecondDmitroEducation extends ExperienceBuilder {

	@Override
	public void buildIdExperience() {
		experience.setIdExperience(2);
		
	}

	@Override
	public void buildStart() {
		experience.setStart(LocalDate.parse("2007-09-01", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildEnd() {
		experience.setEnd(LocalDate.parse("2012-07-01", DateTimeFormatter.ISO_LOCAL_DATE));
		
	}

	@Override
	public void buildPlace() {
		experience.setPlace("Dhnu");
		
	}
	

}
